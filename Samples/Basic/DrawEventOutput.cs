using UnityEngine;

namespace Painter2D.Samples.Basic
{
    public class DrawEventOutput : MonoBehaviour
    {
        [SerializeField] private bool isOutputOn = false;
        private PaintCanvas _paintCanvas;

        private void Awake()
        {
            _paintCanvas = FindObjectOfType<PaintCanvas>();
        }

        public void OnDrawEvent(PaintCanvas.DrawEventType type)
        {
            if (isOutputOn)
            {
                Debug.Log(
                    $"event type: {type}, " +
                    $"mouse pos: {_paintCanvas.CurrentMousePos}, " +
                    $"on canvas: {_paintCanvas.CurrentScaledCanvasPos}");
            }
        }
    }
}