﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Painter2D
{
    public class PaintCanvas : MonoBehaviour
    {
        public RawImage paintTarget;
        [CanBeNull] public Material penMat;
        public Texture2D penTexture;
        public float penScale = 1f;
        private float PenScale => Mathf.Clamp(penScale, 0f, Mathf.Abs(penScale));
        public Color penColor = Color.white;
        private const float LerpDrawDamp = 0.02f;

        private RectTransform _canvasRect;

        public float CanvasWidth { get; private set; }
        public float CanvasHeight { get; private set; }

        private RenderTexture _renderTex;

        private Vector2 _currentMousePos;
        public Vector2 CurrentMousePos => _currentMousePos;
        public Vector2 LastMousePos { get; private set; }
        public Vector3 CurrentScaledCanvasPos { get; private set; }
        public Vector3 LastScaledCanvasPos { get; private set; }

        public CanvasBounds CurrentBounds { get; private set; }

        private bool IsMousePosOnCanvas => _canvasRect.rect.Contains(CurrentMousePos);

        private Rect _uvRect = new Rect(0, 0, 1, 1);

        public readonly UnityEvent<RenderTexture> onDrawTexture = new();
        public readonly UnityEvent<DrawEventType> onDrawEvent = new();

        private void Awake()
        {
            if (paintTarget == null) throw new MissingReferenceException("paintTarget must not be null");
        }

        private void Start()
        {
            SetupCanvas();
            ResetCanvas();
        }

        private void SetupCanvas()
        {
            _canvasRect = GetComponent<RectTransform>();

            var rect = _canvasRect.rect;
            CanvasWidth = rect.width;
            CanvasHeight = rect.height;

            CurrentBounds = new CanvasBounds(CanvasWidth, 0, CanvasHeight, 0);

            _renderTex = new RenderTexture((int) CanvasWidth, (int) CanvasHeight, 0, RenderTextureFormat.ARGB32)
            {
                filterMode = FilterMode.Bilinear,
                useMipMap = false
            };

            if (paintTarget.color != penColor) paintTarget.color = penColor;
            paintTarget.texture = _renderTex;
            if (penMat != null) penMat.color = penColor;
            // penMat.mainTexture = penTexture;
        }

        private void Update()
        {
            LastMousePos = CurrentMousePos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvasRect,
                Input.mousePosition, null,
                out _currentMousePos);
            if (!IsMousePosOnCanvas) LastMousePos = CurrentMousePos;
            CurrentScaledCanvasPos = GetScaledCanvasPos(CurrentMousePos);
            LastScaledCanvasPos = GetScaledCanvasPos(LastMousePos);
            if (Input.GetMouseButtonDown(0) && IsMousePosOnCanvas)
            {
                LastMousePos = CurrentMousePos;
                DrawOnClick();
                CurrentBounds = CurrentBounds.FromPosition(CurrentScaledCanvasPos);
                onDrawTexture.Invoke(_renderTex);
                onDrawEvent.Invoke(DrawEventType.Start);
            }

            if (Input.GetMouseButton(0) && IsMousePosOnCanvas)
            {
                DrawOnDrag();
                CurrentBounds = CurrentBounds.FromPosition(CurrentScaledCanvasPos);
                onDrawTexture.Invoke(_renderTex);
                onDrawEvent.Invoke(DrawEventType.Drag);
            }

            if (Input.GetMouseButtonUp(0))
            {
                EndDraw();
                onDrawEvent.Invoke(DrawEventType.End);
            }
        }

        private void DrawOnClick()
        {
            var brushWidth = penTexture.width * PenScale;
            var brushHeight = penTexture.height * PenScale;
            var brushRect = new Rect(CurrentScaledCanvasPos.x - brushWidth * 0.5f,
                CurrentScaledCanvasPos.y - brushHeight * 0.5f, brushWidth,
                brushHeight);
            GL.PushMatrix();
            GL.LoadPixelMatrix(0, _renderTex.width, _renderTex.height, 0);
            RenderTexture.active = _renderTex;
            Graphics.DrawTexture(brushRect, penTexture, penMat);
            RenderTexture.active = null;
            GL.PopMatrix();
        }

        private void DrawOnDrag()
        {
            if (CurrentMousePos == LastMousePos) return;
            if (penMat != null) penMat.color = penColor;
            GL.PushMatrix();
            GL.LoadPixelMatrix(0, _renderTex.width, _renderTex.height, 0);
            RenderTexture.active = _renderTex;
            LerpDraw(CurrentScaledCanvasPos, LastScaledCanvasPos);
            RenderTexture.active = null;
            GL.PopMatrix();
        }

        private void LerpDraw(Vector3 current, Vector3 prev)
        {
            var distance = Vector2.Distance(current, prev);
            if (!(distance > 0f)) return;
            var tempPos = Vector2.zero;
            var scaledPenWidth = penTexture.width * PenScale;
            var scaledPenHeight = penTexture.height * PenScale;
            var lerpDamp = Mathf.Min(scaledPenWidth, scaledPenHeight) * LerpDrawDamp;
            _uvRect.width = CanvasWidth;
            _uvRect.height = CanvasHeight;
            for (float i = 0; i < distance; i += lerpDamp)
            {
                var lDelta = i / distance;
                var lDifX = current.x - prev.x;
                var lDifY = current.y - prev.y;
                tempPos.x = prev.x + (lDifX * lDelta);
                tempPos.y = prev.y + (lDifY * lDelta);
                var rect = new Rect(tempPos.x - scaledPenWidth * 0.5f, tempPos.y - scaledPenHeight * 0.5f,
                    scaledPenWidth, scaledPenHeight);
                if (Intersect(ref _uvRect, ref rect))
                {
                    Graphics.DrawTexture(rect, penTexture, penMat);
                }
            }
        }

        private void EndDraw()
        {
            // penColor = Random.ColorHSV();
            // if(Application.platform == RuntimePlatform.Android){
            // RenderTexture.active = renderTex;
            // Texture2D temp = new Texture2D((int) canvasWidth, (int) canvasHeight);
            // temp.ReadPixels(new Rect(0, 0, canvasWidth, canvasHeight), 0, 0);
            // temp.Apply();
            // RenderTexture.active = null;
            // Debug.Log($"setup new texture with width: {temp.width}, height: {temp.height}");
            //
            // int scaledWidth = (int) (canvasWidth * 0.05f);
            // int scaledHeight = (int) (canvasHeight * 0.05f);
            // TextureScale.Point(temp, scaledWidth, scaledHeight);
            // Debug.Log($"scaled down out-texture to w: {temp.width}, h: {temp.height}");

            // digitClassifier?.classify(scaledWidth, scaledHeight, temp.GetRawTextureData<sbyte>().ToArray());
            // }
        }

        public void ResetCanvas(bool keepImage = false)
        {
            Graphics.SetRenderTarget(_renderTex);
            GL.Clear(true, true, new Color(0, 0, 0, 0));
            RenderTexture.active = null;
        }

        public RawImage DuplicateCanvas()
        {
            var copyTexture = new Texture2D(paintTarget.texture.width, paintTarget.texture.height, TextureFormat.ARGB32,
                false);
            var duplicate = Instantiate(paintTarget.gameObject, paintTarget.transform.parent).GetComponent<RawImage>();
            Graphics.CopyTexture(paintTarget.texture, copyTexture);
            duplicate.texture = copyTexture;
            return duplicate;
        }

        private Vector3 GetScaledCanvasPos(Vector3 pos)
        {
            var uvPos = Rect.PointToNormalized(_canvasRect.rect, pos);
            return new Vector3(uvPos.x * CanvasWidth, CanvasHeight - uvPos.y * CanvasHeight, 0);
        }

        private static bool Intersect(ref Rect a, ref Rect b)
        {
            var c1 = a.xMin < b.xMax;
            var c2 = a.xMax > b.xMin;
            var c3 = a.yMin < b.yMax;
            var c4 = a.yMax > b.yMin;
            return c1 && c2 && c3 && c4;
        }

        public enum DrawEventType
        {
            Start,
            Drag,
            End
        }

        public readonly struct CanvasBounds
        {
            public readonly float xMin;
            public readonly float xMax;
            public readonly float yMin;
            public readonly float yMax;

            public float xDiff => xMax - xMin;
            public float yDiff => yMax - yMin;

            public Vector2 vMin => new Vector2(xMin, yMin);
            public Vector2 vMax => new Vector2(xMax, yMax);

            public CanvasBounds(float xMin, float xMax, float yMin, float yMax)
            {
                this.xMin = xMin;
                this.xMax = xMax;
                this.yMin = yMin;
                this.yMax = yMax;
            }

            public CanvasBounds CloneWith(int xMin = default, int xMax = default, int yMin = default,
                int yMax = default)
            {
                return new CanvasBounds(
                    xMin == default ? this.xMin : xMin, xMax == default ? this.xMax : xMax,
                    yMin == default ? this.yMin : yMin, yMax == default ? this.yMax : yMax);
            }

            public CanvasBounds FromPosition(Vector2 pos)
            {
                return new CanvasBounds(
                    pos.x < xMin ? pos.x : xMin,
                    pos.x > xMax ? pos.x : xMax,
                    pos.y < yMin ? pos.y : yMin,
                    pos.y > yMax ? pos.y : yMax
                );
            }

            public override string ToString()
            {
                return $"xMin: {xMin}, xMax: {xMax}, yMin: {yMin}, yMax: {yMax}";
            }
        }
    }
}