﻿# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.5] - 2022-05-20
### Changed
- Upgraded Unity version to 2022.1.1f1
- Updated namespaces

## [1.0.4] - 2021-12-17
### Added
- Painter can now duplicate the canvas for persistence

## [1.0.3] - 2021-12-15
### Fixed
- Fixed line jumps after mouse was outside of viewport

### Changed
- Paint Canvas no longer needs a RawImage component on the same object
- Upgraded Unity to 2021.2.6f1

## [1.0.2] - 2021-08-18
### Added
- Canvas bounds for determining painted area

### Fixed
- Fixed painter drawing on edge of canvas even though mouse not on canvas

### Changed
- Upgraded Unity to 2021.1.17f1
- Simplified painter property access

### Removed
- Drawing mode

## [1.0.1] - 2021-08-15
### Added
- Drawing mode for manual control
- Pen scale
- Proper CHANGELOG

### Changed
- Simplified script

## [1.0.0] - 2021-08-07
### Added
- Basic script for drawing
- Sample scenes, textures & shaders for implementation
- Added README & LICENSE

[Unreleased]: https://gitlab.com/Nomad404/Unity-Painter2D/compare/1.0.5...HEAD
[1.0.5]: https://gitlab.com/Nomad404/Unity-Painter2D/compare/1.0.5...1.0.4
[1.0.4]: https://gitlab.com/Nomad404/Unity-Painter2D/compare/1.0.4...1.0.3
[1.0.3]: https://gitlab.com/Nomad404/Unity-Painter2D/compare/1.0.3...1.0.2
[1.0.2]: https://gitlab.com/Nomad404/Unity-Painter2D/compare/1.0.2...1.0.1
[1.0.1]: https://gitlab.com/Nomad404/Unity-Painter2D/compare/1.0.1...1.0.0
[1.0.0]: https://gitlab.com/Nomad404/Unity-Painter2D/releases/tag/1.0.0